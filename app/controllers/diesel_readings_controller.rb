class DieselReadingsController < ApplicationController
	before_action :authenticate_user! 
#		  respond_to :html, :js

#  def index
#  end

  def show
  end

  def create
		@diesel_reading = DieselReading.new(diesel_reading_params)
		dieselPrice = 46
		client_details = ClientGenSet.where(id: params[:diesel_reading][:client_gen_set_id])
		fetched_client_detail_id = '0'
    client_details.each do |object|
			fetched_client_detail_id = object.client_detail_id
		end

		old_value = DieselReading.where(client_gen_set_id: params[:diesel_reading][:client_gen_set_id]).order("created_at").last
		if not old_value.blank?
  		if (@diesel_reading.remaining_fuel <= old_value.remaining_fuel && @diesel_reading.remaining_fuel > 0)
		@gen_expense = GenExpense.new
		@gen_expense.date = Date.today
		@gen_expense.reading = ""
		@gen_expense.readingBy = "Self"
		@gen_expense.batteryStatus = ""
		@gen_expense.radiatorStatus = ""
		@gen_expense.added_diesel_charges = '0'
		@gen_expense.otherExpenses = '0'
		@gen_expense.client_detail_id = fetched_client_detail_id 
   		@gen_expense.consumed_diesel_charges = (old_value.remaining_fuel - @diesel_reading.remaining_fuel) * dieselPrice
#		debug @gen_expense
		@gen_expense.save!
  			respond_to do |format|

				if @diesel_reading.save
					format.html {render json: @gen_expense.id, notice: 'Client was successfully created.' }
					format.json {render json: @diesel_reading, status: :ok}
			  else
				  format.html { render action: 'new' }
					format.json { render json: @diesel_reading.errors, status: :unprocessable_entity }
			  end

 	      end
	   end

		else

			respond_to do |format|

				if @diesel_reading.save
					format.html { redirect_to @diesel_reading, notice: 'Client was successfully created.' }
					format.json {render json: @diesel_reading, status: :ok}
			  else
				  format.html { render action: 'new' }
					format.json { render json: @diesel_reading.errors, status: :unprocessable_entity }
  		  end

		  end

 end
	end

	def diesel_reading_params
		params.require(:diesel_reading).permit(:remaining_fuel, :added_fuel, :client_gen_set_id)
	end
end
