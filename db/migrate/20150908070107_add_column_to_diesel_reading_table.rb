class AddColumnToDieselReadingTable < ActiveRecord::Migration
  def change
		add_column :gen_expenses, :consumed_diesel_charges, :numeric
		rename_column :gen_expenses, :dieselCharge, :added_diesel_charges
  end
end
