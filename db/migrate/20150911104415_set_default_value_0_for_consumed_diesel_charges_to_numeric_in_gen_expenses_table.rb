class SetDefaultValue0ForConsumedDieselChargesToNumericInGenExpensesTable < ActiveRecord::Migration
  def change
		change_column_default(:gen_expenses, :consumed_diesel_charges, 0)
  end
end
